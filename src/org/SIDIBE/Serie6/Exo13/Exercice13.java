package org.SIDIBE.Serie6.Exo13;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercice13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//************question1*******************************
		List<String> chaines = Arrays.asList("one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
				"ten", "eleven", "twelve");
		Stream<String> strings = chaines.stream();
		Comparator<String> cmpLongueur = Comparator.comparingInt(s -> s.length());
		System.out.println("**************Question1 : *****************");
		System.out.println("La longeur de la chaine la plus longue est: " + strings.max(cmpLongueur).get().length());
		
		//***************question 2*****************************
		//on ne pourra pas reutiliser notre stream "strings" donc on applique directement stream a
		//notre liste
		long chainePaire = chaines.stream().filter(s -> s.length() % 2 == 0).count();
		System.out.println("*************Question2************");
		System.out.println(" nombre de chaine paires: " + chainePaire);
		
		//***************Question 3****************************
		List<String> chaineImpaire = chaines.stream().filter(s -> s.length() % 2 != 0).map(s -> s.toUpperCase())
				.collect(Collectors.toList());
		System.out.println("*************Question3************");
		System.out.println(chaineImpaire);
		
		//***************Question 4***************************
		String concatenation=chaines.stream().filter(s->s.length()<=5).sorted().collect(Collectors.joining(",","{","}"));
		System.out.println("*************Question4************");
		System.out.println(concatenation);
		
		//******************Question 5*************************
		Map<Integer,List<String>> tableHachageT1=chaines.stream()
				.collect(Collectors.groupingBy(s->s.length()));
		System.out.println("**************Question5***********");
		System.out.println(tableHachageT1);
		
		//*****************Question 6************************
		Map<String,String> tableHachageT2=chaines.stream()
				.collect(Collectors.groupingBy(s->s.substring(0,1),Collectors.joining(",")));
		System.out.println("***********Question 6 :**************");
		System.out.println(tableHachageT2);
		
		
	}

}
