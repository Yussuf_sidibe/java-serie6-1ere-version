package org.SIDIBE.Serie6.Exo14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercice14 {

	private static List<String> readGerminal() {
		URL url = null;
		int responseCode = 0;
		try {
			url = new URL("https://ia600502.us.archive.org/10/items/germinal05711gut/7germ10.txt");

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");

			responseCode = connection.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			List<String> lines = new ArrayList<>();

			while ((inputLine = in.readLine()) != null) {
				lines.add(inputLine);
			}
			in.close();

			return lines;

		} catch (IOException e) {
			System.out.println("Impossible to read Germinal, responseCode is " + responseCode);
			System.out.println("Error message is " + e.getMessage());
		}

		return Collections.emptyList();
	}

	public static List<String> germinalLines() {
		List<String> germinal = readGerminal();
		int taille = germinal.size();
		return germinal.stream().limit(taille - 322).skip(70).collect(Collectors.toList());

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> germinal = germinalLines();// on recupere les lignes de
												// germinal
		// System.out.println(germinal);

		// **********************Question 2
		long nbreLignesNonVide_bis = germinal.stream().map(s -> s.replaceAll(" ", "")).filter(l -> l.length() != 0)
				.count();

		System.out.println("*******Question 2 : ************");
		System.out.println("Le nombre de lignes non vide_bis est: " + nbreLignesNonVide_bis);

		// *******************Question 3
		Function<String, Integer> nombreDeBonjour = s -> {
			int nbre = 0;

			BiFunction<String, String, Integer> nombreDeFois = (line, chaine) -> {
				int i = 0, index = line.toUpperCase().indexOf(chaine);
				while (index >= 0) {
					i += 1;
					index = line.indexOf(chaine, index + 1);
				}
				return i;
			};
			for (String line : germinal) {
				line.toUpperCase();
				nbre = nbre + nombreDeFois.apply(line, "BONJOUR");
			}
			return nbre;
		};
		System.out.println("**********Question 3 : ***********");
		System.out.println("Le nombre_bis  'bonjour' dans le texte est: " + nombreDeBonjour.apply(germinal.toString()));

		// *********************Question 4 *****************
		Function<String, Stream<Character>> streamChar = chaine -> chaine.chars().mapToObj(lettre -> (char) lettre);

		System.out.println("**********Question 4 : ************");
		System.out.println(streamChar.apply("Yussuf;julio0;::;;awa;youma").collect(Collectors.toList()));

		// *********************Question 5 ****************
		Function<List<String>, Stream<Character>> streamChar1 = line -> line.stream().flatMap(streamChar);

		System.out.println("**********Question 5 : ************");
		// System.out.println(streamChar1.apply(germinal).collect(Collectors.toList()));

		// ********************Question 6 *******************
		List<Character> listCharacter = streamChar1.apply(germinal).distinct().sorted().collect(Collectors.toList());

		System.out.println("**********Question 6 : *************");
		System.out.println(listCharacter);
		System.out.println(listCharacter.size() + " characteres");

		// *******************BiFunction<String, String, Stream<String>>
		// splitWordWithPattern ********************
		// ***donn� par le prof et permet d'effectuer un split

		BiFunction<String, String, Stream<String>> splitWordWithPattern = (line, pattern) -> Pattern
				.compile("[" + pattern + "]").splitAsStream(line);

		// *******************Question 7 ********************
		String separateur = " '.;:?!16<>257084,K9*_\\-";
		Function<List<String>, Stream<String>> streamString = line -> line.stream()
				.flatMap(str -> splitWordWithPattern.apply(str, separateur)).filter(str -> str.length() != 0);

		int nombreTotalMots = streamString
				.apply(germinal)
				.collect(Collectors.toList())
				.size();
		int nombreMotDifferents = streamString
				.apply(germinal)
				.distinct()
				.collect(Collectors.toList())
				.size();
		System.out.println("***********Question 7 : ************");
		System.out.println("Le nombre de mots dans Germinal est: " + nombreTotalMots);
		System.out.println("Le nombre de mots differents  dans Germinal est: " + nombreMotDifferents);

		// *******************Question 8 ***********************
		int tailleMotPlusLong = streamString
				.apply(germinal)
				.map(str -> str.length())
				.max(Comparator.naturalOrder())
				.get();
		
		List<String> listMot = streamString.apply(germinal).distinct().collect(Collectors.toList());
		Map<Integer, List<String>> map = listMot.stream().collect(Collectors.groupingBy(s -> s.length()));
		int nombreMotMax = map.get(tailleMotPlusLong).size();
		System.out.println("**********Question 8 : ************");
		System.out.println("La taille du mot le plus long est: " + tailleMotPlusLong);
		System.out.println("Le nombre de mot ayant comme taille la taille max est: " + nombreMotMax);
		System.out.println("les mots les plus longs dans Germinal sont: " + map.get(tailleMotPlusLong));

		// ********************Question 9 *********************
		Map<Integer, Long> histogramme = listMot
				.stream()
				.filter(str -> str.length() >= 2)
				.collect(Collectors.groupingBy(s -> s.length(), Collectors.counting()));
		System.out.println(histogramme);
		Entry<Integer, Long> plusFrequent = histogramme
				.entrySet()
				.stream()
				.max(Map.Entry.comparingByValue())
				.get();
		System.out.println("********Question 9 : ************");
		System.out.println("La longueur de mot la plus frequente est: "+plusFrequent.getKey()+"  lettres");
		System.out.println("Le nombre de mots  ayant cette longeur est : "+plusFrequent.getValue()+" mots");
		
		System.out.println(plusFrequent);
		
		//**********************Question 10 *****************
		
		int nombreMots=0;
		int cleMediane=0;
		int valeurMediane=0;
		boolean medianeTrouve=false;
		for(Entry<Integer, Long> entree:histogramme.entrySet())
			nombreMots=(int) (nombreMots+entree.getValue());
		//System.out.println(nombreMots);
		for(Entry<Integer, Long> entree:histogramme.entrySet())
		{
			if(valeurMediane<nombreMots/2 && valeurMediane!=-1)
				valeurMediane=valeurMediane+entree.getValue().intValue();
			if(valeurMediane>=nombreMots/2 && medianeTrouve==false)
			{
				cleMediane=entree.getKey();
				valeurMediane=-1;
				medianeTrouve=true;
			}
		}
		System.out.println("La taille de mot mediane est :"+cleMediane);
		System.out.println("Le nombre de mots associes a cette taille est: "+histogramme.get(cleMediane));
		
		

	}

}
